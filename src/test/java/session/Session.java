package session;

import factoryBrowser.FactoryBrowser;
import org.openqa.selenium.WebDriver;

public class Session {
    private static Session sesion=null;
    private WebDriver driver;
    private Session(){
        driver= FactoryBrowser.make("proxy").create();
    }

    public static Session getInstance(){
        if (sesion == null)
            sesion= new Session();
        return  sesion;
    }

    public void closeSesion(){
        driver.quit();
        sesion=null;
    }

    public WebDriver getDriver() {
        return driver;
    }
}