package testcases;

import io.qameta.allure.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pageObject.LoginModal;
import pageObject.MainPage;
import session.Session;

public class TodoistTest {

    MainPage mainPage = new MainPage();
    LoginModal loginModal = new LoginModal();

    @BeforeEach
    public void before(){
        Session.getInstance().getDriver().get("https://todoist.com/");
    }


    @DisplayName("Verificar la ejecucion de el login")
    @Description("Este test cases es para verifcar ......")
    @Link("jira.copm/345345")
    @Issue("Bug000052")
    @Severity(SeverityLevel.CRITICAL)
    @Test
    public void verifyTheProjectIsLogged(){
        mainPage.loginButton.click();
        loginModal.emailTextBox.setValue("mpereyraserrate@noexiste.com");
        loginModal.passwordTextBox.setValue("maura2021");
        loginModal.loginButton.click();

        // verificaciones....

    }

    @AfterEach
    public void after(){
        Session.getInstance().closeSesion();
    }


}
